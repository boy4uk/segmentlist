﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;

namespace SegmentList_n
{
    public class Node
    {
        public Segment Value { get; set; }
        public Node Next { get; set; }

        public Node(Segment value) => Value = value;
    }

    public class SegmentList : IEnumerable<Segment>
    {
        public Node Head { get; private set; }
        public Node Tail { get; private set; }
        public bool IsEmpty { get { return Head == null; } }

        public int Count { get; set; }


        public SegmentList()
        {
            Head = Tail = null;
            Count = 0;
        }

        public SegmentList(params Segment[] amountOfSegments)
        {
            foreach (var e in amountOfSegments)
                Insert(new Segment(e.X1, e.Y1, e.X2, e.Y2));
        }

        public SegmentList(string fileName)
        {
            Count = 0;
            Head = Tail = null;
            var streamReader = new StreamReader(fileName);
            var data = streamReader.ReadLine();
            while (data != null)
            {
                var secondaryArray = data.Split(',');
                Insert(new Segment(
                    Convert.ToInt32(secondaryArray[0]),
                    Convert.ToInt32(secondaryArray[1]),
                    Convert.ToInt32(secondaryArray[2]),
                    Convert.ToInt32(secondaryArray[3])
                ));
                data = streamReader.ReadLine();
            }
        }

        public void Show()
        {
            if (IsEmpty) return;
            foreach (var segment in this)
                Console.WriteLine(segment.ToString());
        }

        public void Insert(Segment segment)
        {
            if (segment == null)
                throw new ArgumentException("Wrong argument");
            Node newNode = new Node(segment);
            if (Head == null)
                Head = Tail = newNode;
            else
            {
                foreach (var e in this)
                    if (e.Equals(segment))
                        return;
                Tail.Next = newNode;
                Tail = newNode;
            }
            Count++;
        }

        public SegmentList AngleList()
        {
            var resultList = new SegmentList();

            foreach (var e in this)
                if (e.GetAngle().Equals(Math.PI / 4))
                    resultList.Insert(new Segment(e.X1, e.Y1, e.X2, e.Y2));

            return resultList;
        }

        public SegmentList LengthList(int a, int b)
        {
            if (a > b)
                throw new ArgumentException("Wrong argument");
            var resultList = new SegmentList();
            foreach (var segment in this)
                if (segment.X1 >= a && segment.X2 <= b)
                    resultList.Insert(new Segment(segment.X1, segment.Y1, segment.X2, segment.Y2));
            return resultList;
        }

        public void BubbleSort()
        {
            for (int i = 0; i < Count; i++)
                for (int j = 0; j < Count - 1; j++)
                    if (this[j].Value.Length > this[j + 1].Value.Length)
                    {
                        Segment temp = this[j].Value;
                        this[j].Value = this[j + 1].Value;
                        this[j + 1].Value = temp;
                    }
        }

        public IEnumerator<Segment> GetEnumerator()
        {
            var current = Head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public Node this[int index]
        {
            get
            {
                if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
                var segmentResult = Head;
                for (int i = 0; i < index; i++)
                    segmentResult = segmentResult.Next;
                return segmentResult;

            }
            private set
            {
                if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SegmentList)) return false;
            var list = obj as SegmentList;
            if (list.Count != Count) return false;
            for (int i = 0; i < Count; i++)
                if (!(this[i].Value.Equals(list[i].Value)))
                    return false;
            return true;
        }
    }
}
