﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegmentList_n
{
    class Program
    {
        static void Main(string[] args)
        {
            SegmentList mySegmentList = new SegmentList("Input.txt");
            Console.Write("Show():\n\n");
            mySegmentList.Show();

            Console.Write("List, after Insert(0,0,4,10) 2 times:\n\n");
            mySegmentList.Insert(new Segment(0, 0, 4, 10));
            mySegmentList.Insert(new Segment(0, 0, 4, 10));
            mySegmentList.Show();

            Console.Write("LengthList(0,2):\n\n");
            mySegmentList.LengthList(0,2).Show();

            Console.Write("AngleList():\n\n");
            mySegmentList.AngleList().Show();

            Console.Write("List after Sort():\n\n");
            mySegmentList.BubbleSort();
            mySegmentList.Show();

            Console.ReadKey();
        }
    }
}
