﻿using System;

namespace SegmentList_n
{
    public class Segment
    {
        public int X1 { get; private set; }
        public int Y1 { get; private set; }
        public int X2 { get; private set; }
        public int Y2 { get; private set; }

        public double Length { get { return Math.Sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1)); } }

        public Segment(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }

        public double GetAngle()
        {
            if (X2 - X1 == 0)
                return Math.PI / 2;
            return Math.Atan((double)(Y2 - Y1) / (X2 - X1));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Segment)) return false;
            var segment = obj as Segment;
            return (X1 == segment.X1 && Y1 == segment.Y1 &&
                X2 == segment.X2 && Y2 == segment.Y2);
        }

        public override string ToString()
        {
            return $"X1 = {X1} Y1 = {Y1} \n"
                    + $"X2 = {X2} Y2= {Y2} \n"
                    + $"Length = {Length}\n";
        }
    }
}
