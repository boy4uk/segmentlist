﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SegmentList_n;
using System.Text;

namespace SegmentList_Tests
{
    [TestClass]
    public class SegmentListTests
    {
        [TestMethod]
        public void Show_test()
        {
            var list = new SegmentList();
            var result = new StringBuilder();
            list.Insert(new Segment(1, 1, 4, 5));
            foreach (var segment in list)
                result.Append(segment.ToString());
            Assert.AreEqual(result.ToString(),
                $"X1 = 1 Y1 = 1 \n"
                    + $"X2 = 4 Y2= 5 \n"
                    + $"Length = 5\n");
        }

        [TestMethod]
        public void Show_test_InsertNullValue()
        {
            var list = new SegmentList();
            var result = new StringBuilder();
            foreach (var segment in list)
                result.Append(segment.ToString());
            Assert.AreEqual(result.ToString(),"");
        }

        [TestMethod]
        public void Insert_Test()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 3, 5));
            list.Insert(new Segment(0, 0, 3, 5));
            list.Insert(new Segment(1, 1, 3, 5));
            list.Insert(new Segment(1, 1, 3, 5));
            list.Insert(new Segment(1, 6, 10, 5));
            Assert.AreEqual(list.Count, 3);
        }


        [TestMethod]
        public void GetAngle_Test_45_Deg()
        {
            var segment = new Segment(-1, -1, 0, 0);
            Assert.AreEqual(segment.GetAngle(), Math.PI / 4);
        }

        [TestMethod]
        public void GetAngle_Test_90_Deg()
        {
            var segment = new Segment(0, 0, 0, 1);
            Assert.AreEqual(segment.GetAngle(), Math.PI / 2);
        }

        [TestMethod]
        public void GetAngle_Test_0_Deg()
        {
            var segment = new Segment(0, 0, 1, 0);
            Assert.AreEqual(segment.GetAngle(), 0);
        }

        /*
         * Прямоугольный реугольник не может иметь угол в 30' и при этом иметь целочисленные координаты вершин.
         * Док-во:
         * Th.: Сторона против угла в 30' = половине гипотенузы =>
         * => 
         * Sqrt(a^2 + b^2) = 2a
         * a^2+b^2=4a^2
         * b^2=3a^2
         * b = a*Sqrt(3)
         * 
         * Таким образом, b будет целым числом тогда и только тогда, когда a = k*Sqrt(3) , где k прин. Z,
         * что противоречит условию. чтд
         * 
         * Поэтому тесты на это писать не буду
         */

        [TestMethod]
        public void AngleList_Test_Basic()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 3, 5));
            list.Insert(new Segment(1, 1, 5, 5));
            list.Insert(new Segment(-1, -1, 1, 1));

            Assert.AreEqual(list.AngleList(), new SegmentList(new Segment(1, 1, 5, 5), new Segment(-1, -1, 1, 1)));
        }

        [TestMethod]
        public void AngleList_Test_ListHaveNoSegments()
        {
            var list = new SegmentList();
            Assert.AreEqual(list.AngleList().Count, 0);

        }

        [TestMethod]
        public void AngleList_Test_AllSegmentsAreWrong()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 1, 0));
            list.Insert(new Segment(0, 0, 0, 1));
            list.Insert(new Segment(0, 0, -1, 1));
            list.Insert(new Segment(0, 0, -1, 0));
            Assert.AreEqual(list.AngleList().Count, 0);
        }

        [TestMethod]
        public void LengthList_Test()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 3, 5));
            list.Insert(new Segment(1, 1, 5, 5));
            list.Insert(new Segment(-1, -1, 1, 1));
            list.Insert(new Segment(-2, 0, 3, 5));
            list.Insert(new Segment(1, 2, 5, 5));
            list.Insert(new Segment(-6, -1, 3, 1));
            Assert.AreEqual(list.LengthList(-2, 3), new SegmentList(new Segment(0, 0, 3, 5), new Segment(-1, -1, 1, 1), (new Segment(-2, 0, 3, 5))));
        }

        [TestMethod]
        public void BubbleSort_Test()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 3, 3));
            list.Insert(new Segment(0, 0, 2, 2));
            list.Insert(new Segment(0, 0, 1, 1));
            list.BubbleSort();
            Assert.AreEqual(list, new SegmentList(new Segment(0, 0, 1, 1), new Segment(0, 0, 2, 2), new Segment(0, 0, 3, 3)));
        }

        [TestMethod]
        public void GetLength_Test()
        {
            var segment0 = new Segment(0, 0, 0, 0);
            var segment1 = new Segment(0, 0, 3, 4);
            var segment2 = new Segment(0, 0, 1, 1);
            var segment3 = new Segment(0, 0, 10, 0);
            Assert.AreEqual(segment0.Length, 0);
            Assert.AreEqual(segment1.Length, 5);
            Assert.AreEqual(segment2.Length, Math.Sqrt(2));
            Assert.AreEqual(segment3.Length, 10);
        }

        [TestMethod]
        public void CreatingExceptions()
        {
            var list = new SegmentList();
            list.Insert(new Segment(0, 0, 0, 0));
            list.Insert(new Segment(0, 0, 0, 0));
            list.Insert(new Segment(0, 0, 0, 0));

            Assert.ThrowsException<ArgumentException>(() =>list.Insert(null));
            Assert.ThrowsException<ArgumentException>(() => list.LengthList(4,1));
            Assert.ThrowsException<IndexOutOfRangeException>(() => list[100]);
        }
    }
}
